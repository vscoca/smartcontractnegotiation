"""
Module containing the main function
"""
import argparse
import ast
import datetime
import collections
from configobj import ConfigObj
import statistics
import utilities
import agents
import evaluation_tools
import dsla_model_generator
import static_nego


def main():
    conf_file = '../data/conf_file_3terms'
    utility_function_id = [0, 1, 2, 3]
    thresholds_model = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]  # TODO 26 create a temp array
    parser = argparse.ArgumentParser(description='SLA negotiation parameters')
    parser.add_argument('consumers', metavar='nc', type=int, default=5, help='Number of consumers')
    parser.add_argument('providers', metavar='np', type=int, default=50, help='Number of providers')
    parser.add_argument('closure', metavar='closure', type=int, default=1,
                        help='Flag to activate the closure mechanism')
    args = parser.parse_args()
    num_providers = args.providers  # number of providers
    num_consumers = args.consumers
    closure = bool(args.closure)
    config = ConfigObj(conf_file, list_values=False)  # Reading the SLA file
    f = open('../out/results_' + str(datetime.datetime.now()).replace(" ", "") + "_terms_" +
             str(len(ast.literal_eval(config['TERMS']['terms'])))+ '.txt', 'w')
    f.write("Number of terms: " + str(len(ast.literal_eval(config['TERMS']['terms']))) + "\n"
            "Terms: " + str(config['TERMS']['terms'])+ "\n")
    results = []
    cp_data = {} #consumer provider data
    c_results = {'results': {0: {}, 1: {}, 2: {}, 3: {}}, 'nego_round': {0: {}, 1: {}, 2: {}, 3: {}}}
    c_edges = 0
    c_nodes = 0
    p_edges = 0
    p_nodes = 0
    for q, e in c_results.iteritems():
        for k, v in e.iteritems():
            for n in thresholds_model:
                v.update({n: []})
                v.update({n: []})
    for i in range(0, num_consumers):
        consumer = agents.Negotiatior('consumer', ast.literal_eval(config['RANGES']['consumer']),
                                      ast.literal_eval(config['WEIGHTS']['consumer']))
        prov_found = 0
        initial_terms=[]
        for j in range(0, num_providers):
            provider = agents.Negotiatior('provider', ast.literal_eval(config['RANGES']['provider']),
                                          ast.literal_eval(config['WEIGHTS']['provider']))
            staticNego = static_nego.StaticSLANegotiatior(consumer, provider)
            agreed, agreement = staticNego.negotiation_prot()
            if agreed:
                prov_found += 1
                initial_terms.append(agreement)
            else:
                pass
        results.append(prov_found)
        print "Number of agreements established by consumer " + str(i) + " " + str(prov_found)
        cp_data.update({i: initial_terms})
    print "Average number of succesfull negotiation: " + str(statistics.mean(results)) + \
          " (stddev: " + str(statistics.stdev(results)) + ")"
    f.write( "Average number of succesfull static negotiation: " + str(statistics.mean(results)) +
             " (stddev: " + str(statistics.stdev(results)) + ")" + "\n")
    for i in range(0, num_consumers):
        print "Consumer n: " + str(i)
        slas = dsla_model_generator.SlaManagement(cp_data[i], conf_file)
        provider_list_sat, provider_list_no_sat,c_n,c_e,p_n,p_e = slas.instantiate_slas(closure)
        c_nodes += c_n
        c_edges += c_e
        p_nodes += p_n
        p_edges += p_e
        if len(provider_list_no_sat) != 0 or len(provider_list_sat) != 0:
            results, nego_round = evaluation_tools.results_eval(thresholds_model, utility_function_id,
                                                                provider_list_sat, num_providers, provider_list_no_sat)
            for k, v in results.iteritems():
                for l, m in v.iteritems():
                    c_results['results'][k][l].append(m)
            for k, v in nego_round.iteritems():
                for l, m in v.iteritems():
                    c_results['nego_round'][k][l].append(m)
            c_results.update({i: {'results': results, 'nego_round': nego_round}})
    means_dict = {0:{},1:{},2:{},3:{}}
    stddev_dict = {0: {}, 1: {}, 2: {}, 3: {}}
    nego_round_dict = {0:{},1:{},2:{},3:{}}
    for k,v in c_results['results'].iteritems():
        for l, m in v.iteritems():
            means_dict[k].update({l: statistics.mean(m)})
            stddev_dict[k].update({l: statistics.stdev(m)})
    for k, v in c_results['nego_round'].iteritems():
        for l, m in v.iteritems():
            nego_round_dict[k].update({l: statistics.mean(m)})
    print "Consumer avg_nodes: " + str(float(c_nodes / num_consumers))
    f.write("Consumer avg_nodes: " + str(float(c_nodes / num_consumers)) + "\n")
    print "Consumer avg_edges: " + str(float(c_edges / num_consumers))
    f.write("Consumer avg_edges: " + str(float(c_edges / num_consumers)) + "\n")
    print "Provider avg_nodes: " + str(float(p_nodes / num_consumers))
    f.write( "Provider avg_nodes: " + str(float(p_nodes / num_consumers)) + "\n")
    print "Provider avg_edges: " + str(float(p_edges / num_consumers))
    f.write("Provider avg_edges: " + str(float(p_edges / num_consumers)) + "\n")
    evaluation_tools.print_results(means_dict, f)
    evaluation_tools.print_nego_round(nego_round, 1, f)
    f.write("End time: " + str(datetime.datetime.now()).replace(" ", ""))
    #utilities.plot_results(means_dict, "Average results_terms" + str(len(ast.literal_eval(config['TERMS']['terms']))))
    #utilities.plot_results(nego_round_dict, "Average negotiation round" +
    #                       str(len(ast.literal_eval(config['TERMS']['terms']))))
    #for k,v in means_dict.iteritems():
    #    utilities.plot_errorbar(thresholds_model,collections.OrderedDict(sorted(v.items())).values(),
    #                            collections.OrderedDict(sorted(stddev_dict[k].items())).values(),
    #                            "Results uf " + str(k) +
    #                            "n_terms_" + str(len(ast.literal_eval(config['TERMS']['terms']))))
    f.close()

if __name__ == "__main__": main()
