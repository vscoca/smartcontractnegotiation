"""
A set of utility functions for:

- graph management;
- performance evaluation
- results plotting
"""

import pygraphviz as pgv
from matplotlib import pylab
from matplotlib import ticker
from matplotlib2tikz import save as tikz_save
import itertools
import networkx as nx
import re
import datetime
import matplotlib.pyplot as plt
import numpy as np


def get_nodes_id(nodes):
    """
    Return the ids of a set of nodes
    :param nodes: set of input nodes
    :return: list of node ids
    """
    nodes_id = []
    for i in nodes:
        nodes_id.append(i.get_state_label())
    return nodes_id


def get_ini_nodes(g):
    """
    To get the initial nodes of a graph
    :param g: input graph
    :return: the list of initial nodes (if any)
    """
    ini_nodes = []
    for i in g.nodes():
        if g.in_degree(i) == 0:
            ini_nodes.append(i)
    return ini_nodes


def get_end_nodes(g):
    """
    Get the end-nodes of g
    :param g: The graph
    :return: list of end-node ids
    """
    end_nodes = []
    for i in g.nodes():
        if g.out_degree(i) == 0:
            end_nodes.append(i)
    return end_nodes


def get_agents(sla):
    """
    For each action extract the negotiator and the agent
    :param sla:
    :return:
    """
    sla_parties = {}
    for i in sla:
        sla_parties[i[0]] = (i[3], i[4])  # sla_parties[C1C] = (consumer,consumer)
    return sla_parties


def edge_label_update(edge, terms):
    """
    To generate the new edge label (source, end, old_label)
    :param edge:
    :param terms:
    :return:
    """
    new_label = ""
    for i in terms:
        if i == 'VM':
            regex = str(i)+":\d+"
            ini_vm = int(re.findall(regex, edge[0].get_state_label())[0].split(":")[1])
            end_vm = int(re.findall(regex, edge[1].get_state_label())[0].split(":")[1])
            new_value = end_vm - ini_vm
            new_label += str(i)+"{0:+}".format(new_value)+","
        else:
            regex = str(i) + ":\d+\.\d+"
            ini_rt = float(re.findall(regex, edge[0].get_state_label())[0].split(":")[1])
            end_rt = float(re.findall(regex, edge[1].get_state_label())[0].split(":")[1])
            new_value = end_rt - ini_rt
            new_label += str(i) + "{0:+}".format(new_value)+","
    return new_label[:-1]


def update_edge_nodes(edges, states):
    """
    To update the nodes to the original ones after the function weakly_connected_components_subgraphs
    :param edges:
    :param states:
    :return:
    """
    new_edges = []
    for i in edges:
        new_edges.append((states[i[0].get_state_label()], states[i[1].get_state_label()], i[2]))
    return new_edges


def weakly_connected_components(graph, states):
    """
    To update the nodes of the weakly connected components of the subgraphs
    :param graph:
    :param states:
    :return:
    """
    sg_upd = []
    sub_graphs = list(nx.weakly_connected_component_subgraphs(graph))
    for i in sub_graphs:
        original_sg = nx.MultiDiGraph()
        original_sg.add_edges_from(update_edge_nodes(i.edges(data=True), states))
        sg_upd.append(original_sg)
    return sg_upd


def clean_sparse_nodes(g, ini_state):
    """
    To remove all the sparse nodes in the graph
    :param g: input graph
    :param ini_state: initial state
    :return: the cleaned graph
    """
    for i in g.nodes():
        if i.get_state_label() != ini_state.get_state_label():
            if nx.has_path(g, ini_state, i) == 0:
                g.remove_node(i)
    return g


def edges2nodes(edges):
    """
    To get the set of nodes given a set of edges
    :param edges:
    :return:
    """
    nodes = set([u for (u, v, z) in edges])
    nodes.update(set([v for (u, v, z) in edges]))
    return nodes


def save_graph(g, name, exception=False):
    """
    To save the figure representing the automaton.
    The exception is used for debugging
    :param g: graph representing the automanton
    :param name: filename
    :param exception: Flag that allows or not to save the graph
    :return:
    """
    ag = pgv.AGraph(directed=True, strict=False)
    ag.node_attr['shape'] = 'circle'
    ag.node_attr['height'] = '0.8'
    ag.node_attr['fontsize'] = '5'
    ag.node_attr['fixedsize'] = 'true'
    ag.edge_attr['fontsize'] = '9'
    if exception:
        print " Saving graph: " + str(name)
        for i in g.nodes():
            if i.check_initial():
                ag.add_node(i.get_state_label(), color='green')
            else:
                ag.add_node(i.get_state_label())
            for j in g.out_edges(i, data=True):
                ag.add_edge(j[0].get_state_label(), j[1].get_state_label(),
                            label=str(j[2]['type'])+str(j[2]['label'])+','+str(j[2]['action']))
        ag.layout(prog='dot')
        ag.draw('../out/' + name + str(datetime.datetime.now()).replace(" ", "") + '.png', prog='dot')
    else:
        pass


def mean(value_list):
    """
    Computing the mean
    :param value_list: input values
    :return: mean
    """
    sum = 0
    if len(value_list) != 0:
        for i in value_list:
            sum += i
        mean = float(float(sum)/float(len(value_list)))
    else:
        mean = 0
    return mean


def hmean(value_list):
    """
    Harmonic mean calculation
    :param value_list: input values
    :return: hmean
    """
    sum_value = 0.0
    for i in value_list:
        if i != 0:
            sum_value += float(1/i)
        else:
            hmean = 0.0
            return hmean
    if sum_value != 0.0:
        hmean = float(len(value_list))/sum_value
    else:
        hmean = 0.0
    return hmean


def adaptation_cost(modifications, curr_transitions):
    """
    Compute the adaptation cost as ratio of the modification to be applied over the number of current transitions
    :param modifications: edges to be modified
    :param curr_transitions: number of current transitions
    :return: adapation cost
    """
    return float(float(len(modifications)) / float(curr_transitions))


def generate_combinations(value_list):
    """
    Generate all the possible combinations among the solutions
    :param value_list: list of possible suggestions
    :return: the list of all possible valid combinations among the suggestions
    """
    tot_combinations = 0
    valid_combination = 0
    valid_combination_list = []
    not_valid_combination = 0
    for i in range(1,len(value_list)):
        if tot_combinations < 100000:
            combinations = list(itertools.combinations(value_list,i))
            tot_combinations += len(combinations)
            for j in combinations:
                elements = [n[:-1] for n in j]
                elements_set = set(elements)
                if len(elements_set) < len(elements):
                    not_valid_combination += 1
                else:
                    valid_combination += 1
                    valid_combination_list.append(j)
        else:
            valid_combination_list = []
            break
    return valid_combination_list


def plot_results(results_data, title):
    """
    To plot the results
    :param data: results to be plotted
    :param title: window title
    :return:
    """
    fig, ax = plt.subplots()
    fig.canvas.set_window_title(title)
    ax.yaxis.grid(True, which='major')
    ax.yaxis.set_major_formatter(ticker.FormatStrFormatter('%.1f'))
    marker = itertools.cycle(('^', 's', '>', 'o', '*', '<',))
    colors = itertools.cycle(('r','g','y','b','m'))
    labels = {0: r'$log_{10}(x+1) $', 1: r'$0.5 \times x$', 2: r'$ln(x+1)$', 3: r'$ x^2 $'}
    csfont = {'fontname': 'CMU Bright'}
    csfont_y = {'fontname': 'CMU Bright', 'fontsize' : 9}
    y_ticks = [0.0]
    fig.subplots_adjust(bottom=0.21)  # to get plot and legend within the main window
    for k,v in results_data.iteritems():
        x_values = sorted(v)
        y_values = []
        for l in x_values:
            y_values.append(v[l])
        y_ticks += list(set(y_values))
        y_max = np.amax(y_values)
        plt.ylim(0, y_max+0.5)
        plt.ylabel('providers matched', **csfont)
        plt.xticks(x_values, **csfont_y)
        plt.yticks(list(set(y_ticks)), **csfont_y)
        plt.title(title, **csfont)
        plt.xlabel('Thresholds', **csfont)
        plt.plot(x_values, y_values, marker=marker.next(), label=labels[k], color=colors.next())
        pylab.legend(loc=9, bbox_to_anchor=(0.5, -0.1), ncol=4)
    tikz_save('../out/results_'+'function_'+str(title)+str(datetime.datetime.now()).replace(" ", "")+'.tex')
    plt.show()


def plot_errorbar(x_bar, mean, stddev, title):
    """
    Plot the avg number of matched providers and the std_dev
    :param x_bar: x bar legend
    :param mean: mean values to be plotted
    :param stddev:  stdevs to be plotted
    :param title: window title
    :return:
    """
    fig, ax = plt.subplots()
    fig.canvas.set_window_title(title)
    ax.yaxis.grid(True, which='major')
    ax.yaxis.set_major_formatter(ticker.FormatStrFormatter('%.1f'))
    csfont = {'fontname': 'CMU Bright'}
    csfont_y = {'fontname': 'CMU Bright', 'fontsize': 9}
    y_ticks = [0.0]
    y_ticks += list(set(mean))
    plt.ylabel('Avg Matched Providers', **csfont)
    plt.xticks(x_bar, **csfont_y)
    plt.yticks(list(set(y_ticks)), **csfont_y)
    plt.title(title, **csfont)
    plt.xlabel('Thresholds', **csfont)
    plt.errorbar(x_bar, mean, stddev, linestyle='None', marker='^')
    tikz_save('../out/results_' + str(title) + '_' + str(datetime.datetime.now()).replace(" ", "") + '.tex')
    plt.show()


def graph_statistics(g):
    """
    Counting the number of states and transitions of the graph
    :param g: input graph
    :return: number of nodes and edges
    """
    return len(set(g.nodes())), len(set(g.edges()))