"""
This module contains the class simulationg the static negotiator
"""

import time


class StaticSLANegotiatior():

    def __init__(self, consumer, provider):
        self.parties = {'consumer' : consumer, 'provider' : provider}
        self.nego_on = True
        self.precision = int(1.0e+05)
        self.ini_time = int(round(time.time() * self.precision))


    def negotiation_prot(self):
        """
        This method implements the negotiation protocol
        :return:
        """
        agreement = {}
        agreed = False
        curr_nego = 'consumer'
        opponent = [n for n in self.parties.keys() if n != curr_nego][0]
        curr_offer = self.parties[curr_nego].get_ini_offer()
        while self.nego_on:
            curr_time = int(round(time.time() * self.precision)) - self.ini_time
            if curr_time < self.parties[opponent].get_deadline():
                if self.parties[opponent].offer_evaluation(curr_offer,curr_time):
                    agreed = True
                    self.nego_on = False
                    agreement = curr_offer
                else:
                    curr_offer = self.parties[opponent].offer_generation(curr_time)
                    curr_nego = opponent
                    opponent = [n for n in self.parties.keys() if n != curr_nego][0]
            else:
                self.nego_on = False
        return agreed, dict(agreement)