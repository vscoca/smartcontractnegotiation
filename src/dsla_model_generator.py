import utilities
import networkx as nx
import re
import ast
import random
from configobj import ConfigObj


class SlaState:
    """
    Defining the SLA state
    """

    def __init__(self, terms, initial=False):
        self.state = {}
        self.terms = [k for k,v in terms.iteritems()]
        self.state.update({'INITIAL' : initial, 'TERMS' : terms, 'ID' : self.set_state_label(terms)})

    def get_state_label(self):
        """
        To get the state label as string containing all the term-value
        :return: string
        """
        return str(self.state['ID'])

    def get_terms(self):
        """
        return the dict of the current terms of the SLA
        :return:
        """
        return self.terms

    def get_term_value(self, term):
        """
        To get the current value of the input term
        :param term:
        :return: term value
        """
        return str(self.state['TERMS'][term])

    def get_state(self):
        """
        Get the complete state as dictionary
        :return: the complete state
        """
        state = nx.copy.deepcopy(self.state['TERMS']) #needed to pass only the values not the reference !!!! THE MEMORY ADDRESS CHANGES
        return state

    def check_initial(self):
        """
        A flag to check if a the current state is initial
        :return: Boolean value
        """
        return self.state['INITIAL']

    def set_state_label(self, terms):
        """ To define the state id as concatenation of its variable states VM+RT"""
        id = ""
        for k,v in terms.iteritems():
            id += str(k)+":"+str(v)+","
        return id[:-1]  # to remove last comma

    def set_term(self, term, value):
        """
        To update the term value
        :param term: term to be updated
        :param value: new value
        :return:
        """
        self.state[term] = value


class SlaFsm:
    """
    This class provides the methods for generating the SLA automaton and also the automata for each action in the SLA.
    """
    parties = ['consumer', 'provider']  # lowercase = owner, UPPERCASE = ACTORS

    def __init__(self, sla):
        self.automata = {'consumer': {}, 'provider': {}, 'COMP' : None, 'MATCHED' : None , 'MISSING' : None , 'sat_dict' : {}}  # { consumer: {CONSUMER : graph, PROVIDER: graph, consumerCONSUMERact : {[sub_graph1,...]}, consumerPROVIDERact : {[sub_graph1,...]} }}
        self.sla = sla  # A dictionary containing the SLAs (both consumer and provider sla)
        self.curr_owner = ''  # to set the current sla and the current sla section
        self.curr_section = ''
        self.states = {}    # To keep all the state that are created in the automata building for both the parties(eventually) {state_label : state_instance}
        self.sat_dict = {}

    def sla_fsm(self, ini_state,sat):  # PLEASE NOTE: 'actions' means the whole parsed SLA
        """ To compute the sla dynamics"""
        for j in self.parties:  # SLA owner
            if j in self.sla:   # To check if both consumer and  provider SLA need to be analysed
                sla_agreeement = []
                self.curr_owner = j  # update the current sla and the current sla section
                for i in self.parties:
                    self.curr_section = i.upper()
                    sla_agreeement += (self.sla[self.curr_owner][self.curr_section])
                self.sla_fsm_evolution(ini_state, sla_agreeement, self.sla[self.curr_owner]['INVARIANTS'])  # Defining the automata for the whole SLA
        if sat: #FLAG TO USE OR NOT SATURATION EDGES
            self.sla_fsm_graph_composition_alternative(nx.MultiDiGraph(self.automata['consumer']['SLA']),nx.MultiDiGraph(self.automata['provider']['SLA'])) #SHALLOW COPY: CREATE A NEW GRAPH
        else:
            self.sla_fsm_graph_composition(self.automata['provider']['SLA'],
                                           self.automata['consumer']['SLA'])  # Composition of both SLAs
        return self.automata

    def sla_fsm_get_automata(self):
        """
        To return the automata
        :return:
        """
        return self.automata

    def sla_fsm_graph_composition_alternative(self,g1, g2):
        """
        Computing the compatibility model
        :param g1:
        :param g2:
        :return:
        """
        g2_edges_demand = [n for n in g2.edges(data=True) if n[2]['agent'] == 'provider']
        g1_edges_demand = [n for n in g1.edges(data=True) if n[2]['agent'] == 'consumer']
        g_comp = nx.MultiDiGraph()
        g_match = nx.MultiDiGraph()
        # TODO ADD INITIAL NODE TO THE MATCHING GRAPH 08/12
        for i in g1.nodes():
            if i.check_initial():
                g_match.add_node(i)
        g_miss = nx.MultiDiGraph()
        for i in g1_edges_demand:
            agent = 'consumer'
            consumer =  'consuemr'
            negotiator =  'PROVIDER'
            opponent =  [n for n in self.parties if n != agent][0]
            tmp_src = [n for n in g2.nodes() if i[0].get_state_label() == n.get_state_label()]
            tmp_end = [n for n in g2.nodes() if i[1].get_state_label() == n.get_state_label()]
            if tmp_src and tmp_end:
                g_curr_source = tmp_src[0]
                g_curr_end = tmp_end[0]
                results = self.sla_fsm_compatibility_func_alternative(i,g2)
            else:
                results=[False]
            if results[0]:
                found = True
                if bool(results[1]): # in case of direct matching
                    g_comp.add_edge(i[0], i[1], label=i[2]['label'] + '|' + results[1][0]['label'],
                                    provider=i[2]['label'], consumer=results[1][0]['label'],
                                    agent=i[2]['agent'], type=self.get_action_type(i[2]['type'], results[1][0]['type']),
                                    action=self.edge_label_update((i[0], i[1]), i[1].get_terms()),
                                    negotiator=i[2]['agent'].upper(), act_under=i[2]['act_under'])
                    g_match.add_edge(i[0], i[1], label=i[2]['label'] + '|' + results[1][0]['label'],
                                     provider=i[2]['label'], consumer=results[1][0]['label'],
                                     agent=i[2]['agent'], type=self.get_action_type(i[2]['type'], results[1][0]['type']),
                                     action=self.edge_label_update((i[0], i[1]), i[1].get_terms()),
                                     negotiator=i[2]['agent'].upper(), act_under=i[2]['act_under'])
                else:
                    g_comp.add_edge(i[0], i[1], label=i[2]['label'] + '|' + 'SAT',
                                    provider=i[2]['label'], consumer=consumer,
                                    agent=i[2]['agent'], type='+',
                                    action=self.edge_label_update((i[0], i[1]), i[1].get_terms()),
                                    negotiator=i[2]['agent'].upper(), act_under=i[2]['act_under'])
                    g2.add_edge(
                        g_curr_source,
                        g_curr_end,
                        label='SAT', action=self.edge_label_update((g_curr_source, g_curr_end), g_curr_source.get_terms()), type='+',
                        agent=agent, negotiator=negotiator, act_under=0)
                    g_match.add_edge(i[0], i[1], label=i[2]['label'] + '|' + 'SAT',
                                     provider=i[2]['label'], consumer=consumer,
                                     agent=i[2]['agent'], type= '+',
                                     action=self.edge_label_update((i[0], i[1]), i[1].get_terms()),
                                     negotiator=i[2]['agent'].upper(), act_under=i[2]['act_under'])
            else:
                g_comp.add_edge(i[0], i[1], label=i[2]['label'],
                                agent=i[2]['agent'], type='-',
                                action=self.edge_label_update((i[0], i[1]), i[1].get_terms()),
                                negotiator=i[2]['negotiator'])
                g_miss.add_edge(i[0], i[1], label=i[2]['label'],
                                agent=i[2]['agent'], type='-',
                                action=self.edge_label_update((i[0], i[1]), i[1].get_terms()),
                                negotiator=i[2]['negotiator'])
        for i in g2_edges_demand:
            agent = 'provider'
            consumer = 'consumer'
            negotiator = 'CONSUMER'
            opponent = [n for n in self.parties if n != agent][0]
            tmp_src = [n for n in g1.nodes() if i[0].get_state_label() == n.get_state_label()]
            tmp_end = [n for n in g1.nodes() if i[1].get_state_label() == n.get_state_label()]
            if tmp_src and tmp_end:
                g_curr_source = tmp_src[0]
                g_curr_end = tmp_end[0]
                results = self.sla_fsm_compatibility_func_alternative(i,g1)
            else:
                results=[False]
            if results[0]:
                found = True
                if bool(results[1]):  # in case of direct matching
                    g_comp.add_edge(i[0], i[1], label=i[2]['label'] + '|' + results[1][0]['label'],
                                    provider=i[2]['label'], consumer=results[1][0]['label'],
                                    agent=i[2]['agent'], type=self.get_action_type(i[2]['type'], results[1][0]['type']),
                                    action=self.edge_label_update((i[0], i[1]), i[1].get_terms()),
                                    negotiator=i[2]['agent'].upper(), act_under=i[2]['act_under'])
                    g_match.add_edge(i[0], i[1], label=i[2]['label'] + '|' + results[1][0]['label'],
                                     provider=i[2]['label'], consumer=results[1][0]['label'],
                                     agent=i[2]['agent'],
                                     type=self.get_action_type(i[2]['type'], results[1][0]['type']),
                                     action=self.edge_label_update((i[0], i[1]), i[1].get_terms()),
                                     negotiator=i[2]['agent'].upper(), act_under=i[2]['act_under'])
                else:
                    g_comp.add_edge(i[0], i[1], label=i[2]['label'] + '|' + 'SAT',
                                    provider=i[2]['label'], consumer=consumer,
                                    agent=i[2]['agent'], type='+',
                                    action=self.edge_label_update((i[0], i[1]), i[1].get_terms()),
                                    negotiator=i[2]['agent'].upper(), act_under=i[2]['act_under'])
                    g1.add_edge(
                        g_curr_source,
                        g_curr_end,
                        label='SAT',
                        action=self.edge_label_update((g_curr_source, g_curr_end), g_curr_source.get_terms()), type='+',
                        agent=agent, negotiator=negotiator, act_under=0)
                    g_match.add_edge(i[0], i[1], label=i[2]['label'] + '|' + 'SAT',
                                     provider=i[2]['label'], consumer=consumer,
                                     agent=i[2]['agent'],
                                     type='+',
                                     action=self.edge_label_update((i[0], i[1]), i[1].get_terms()),
                                     negotiator=i[2]['agent'].upper(), act_under=i[2]['act_under'])
            else:
                g_comp.add_edge(i[0], i[1], label=i[2]['label'],
                                agent=i[2]['agent'], type='-',
                                action=self.edge_label_update((i[0], i[1]), i[1].get_terms()),
                                negotiator=i[2]['negotiator'])
                g_miss.add_edge(i[0], i[1], label=i[2]['label'],
                                agent=i[2]['agent'], type='-',
                                action=self.edge_label_update((i[0], i[1]), i[1].get_terms()),
                                negotiator=i[2]['negotiator'])
        self.automata['consumer']['COMPLETE_SLA'] = g1
        self.automata['provider']['COMPLETE_SLA'] = g2
        self.automata.update({'MISSING': g_miss, 'MATCHED': g_match, 'COMP': g_comp})


    #@timecall
    def sla_fsm_graph_composition(self, g1, g2):
        """
        Parallel composition between two SLAs and to keep the missed and matched paths
        :param g1:
        :param g2:
        :return: SLA parallel composition
        """
        #TODO update removing the saturations: provider-based sla.
        #TODO 17/11 I edited the negotiator in such way that negotiator == agent only for the saturation
        g2_edges = g2.edges(data=True)  #consumer sla
        g1_edges = g1.edges(data=True)  #provider sla
        g_comp = nx.MultiDiGraph()
        g_match = nx.MultiDiGraph()
        #TODO ADD INITIAL NODE TO THE MATCHING GRAPH 08/12
        for i in g1.nodes():
            if i.check_initial():
                g_match.add_node(i)
        g_miss = nx.MultiDiGraph()
        for i in g1_edges:
            found = False
            for l in g2_edges:
                #print i[2]
                #print l[2]
                if self.sla_fsm_compatibility_func(i, l):
                    found=True
                    g_comp.add_edge(i[0], i[1], label=i[2]['label'] + '|' + l[2]['label'],
                                    provider=i[2]['label'],consumer=l[2]['label'],
                                    agent=i[2]['agent'], type=self.get_action_type(i[2]['type'],l[2]['type']),
                                    action=self.edge_label_update((i[0],i[1]),i[1].get_terms()) , negotiator=i[2]['agent'].upper(),act_under=i[2]['act_under'])
                    g_match.add_edge(i[0], i[1], label=i[2]['label'] + '|' + l[2]['label'],
                                    provider=i[2]['label'], consumer=l[2]['label'],
                                    agent=i[2]['agent'], type=self.get_action_type(i[2]['type'],l[2]['type']),
                                    action=self.edge_label_update((i[0],i[1]),i[1].get_terms()) , negotiator=i[2]['agent'].upper(), act_under=i[2]['act_under'])
            if not found and i[2]['label'] != 'SAT':
                g_comp.add_edge(i[0], i[1], label=i[2]['label'],
                                agent=i[2]['agent'], type='-',
                                action=utilities.edge_label_update((i[0], i[1]),i[1].get_terms()), negotiator=i[2]['negotiator'])
                g_miss.add_edge(i[0], i[1], label=i[2]['label'],
                                agent=i[2]['agent'], type='-',
                                action=utilities.edge_label_update((i[0], i[1]),i[1].get_terms()), negotiator=i[2]['negotiator'])

        for i in g2_edges:
            found = False
            for j in g1_edges:
                if self.sla_fsm_compatibility_func(i, j):
                    found = True
            if not found and i[2]['label'] != 'SAT':
                g_comp.add_edge(i[0], i[1], label=i[2]['label'],
                                agent=i[2]['agent'], type='-',
                                action=utilities.edge_label_update((i[0], i[1]),i[1].get_terms()), negotiator=i[2]['negotiator'])
                g_miss.add_edge(i[0], i[1], label=i[2]['label'],
                                agent=i[2]['agent'], type='-',
                                action=utilities.edge_label_update((i[0], i[1]),i[1].get_terms()), negotiator=i[2]['negotiator'])
        self.automata.update({'MISSING' : g_miss, 'MATCHED' : g_match, 'COMP' : g_comp})
        utilities.save_graph(g_comp, 'SLA_composition')
        utilities.save_graph(g_miss, 'SLA_miss')
        utilities.save_graph(g_match,'SLA_match')

    def sla_fsm_compatibility_func_alternative(self, edge, g):
        compatibility = False
        direct_edge = {} # edge data in case of direct edge matching
        g_nodes = [n for n in g.nodes()]
        g_nodes_labels = [n.get_state_label() for n in g_nodes ]
        if edge[0].get_state_label() in g_nodes_labels and edge[1].get_state_label() in g_nodes_labels:  # to check if it exists an "endogenous" path
            g_curr_source =  [n for n in g_nodes if edge[0].get_state_label() == n.get_state_label()][0]
            g_curr_end =  [n for n in g_nodes if edge[1].get_state_label() == n.get_state_label() ][0]
            edge_data = g.get_edge_data(g_curr_source,g_curr_end)
            if edge_data != None: # direct edge
                for k,v in edge_data.iteritems():
                    if v['agent'] == edge[2]['agent']:
                        compatibility = True
                        direct_edge = edge_data
                    else:
                        compatibility = False
            else: # no direct edge look for a path
                paths = nx.all_simple_paths(g,g_curr_source,g_curr_end)
                for i in paths:
                    first = True
                    for x in i[:-1]:
                        index = i.index(x)
                        edge_data = g.get_edge_data(x, i[index+1])
                        if first:
                            for k,v in edge_data.iteritems():
                                if v['agent'] == edge[2]['agent']:
                                    compatibility = True #First edge is ok
                                    first = False
                                    break
                        else:
                            for k, v in edge_data.iteritems():
                                if v['agent'] != edge[2]['agent']:
                                    compatibility = False
        else:
            pass
        return compatibility,direct_edge

    def sla_fsm_compatibility_func(self, edge1, edge2):
        """
        Define the compatibility function which allows the graph matching.
        If the state1 and state2 are equal, then we have a positive transition (tau)
        otherwise a deadlock occurs (deadlock state)
        :param state1: state to verified
        :param state2: probe
        :return: tau or deadlock
        """
        compatibility = False
        if edge1[0].get_state_label() == edge2[0].get_state_label() and edge1[1].get_state_label() == edge2[
            1].get_state_label() and edge1[2]['agent'] == edge2[2]['agent']:
            compatibility = True
        else:
            compatibility = False
        return compatibility


    def sla_fsm_condition_eval(self, state, cond):
        """
        From textExp2BoolExpr : (RT lt 8 AND VM lt 8) -> (4 < 8 and 5 < 8)
        :param state:
        :param cond:
        :return:
        """
        op_mapping = {'eq': '==', 'gt': '>', 'geq': '>=', 'leq' : '<=', 'lt': '<', 'AND': 'and', 'OR': 'or'}
        # Substituting the operators
        op_mapping = dict((re.escape(k), v) for k, v in op_mapping.iteritems())
        pattern = re.compile("|".join(op_mapping.keys()))
        cond = pattern.sub(lambda m: op_mapping[re.escape(m.group(0))], cond)
        # Substituting the values in order to get the boolean expression
        s = state.get_state()  # Get only the state variables of SlaState instace
        s = dict((re.escape(k), str(v)) for k, v in s.iteritems()) #TODO 14/11 changed v to str(v)
        pattern = re.compile("|".join(s.keys()))
        cond = pattern.sub(lambda m: s[re.escape(m.group(0))], cond)
        return cond

    def __sla_fsm_state_update(self, old_state, action, invariants):
        """
        To update the state values according to the executed action
        :param old_state: The state to be updated
        :param action: The updating action
        :return: new state value
        """
        #TODO make it more general according to the sla terms
        # Action example: RT 3 AND VM 8 or RT +3 AND VM +8
        new_states = []
        actions_list = re.split('AND|OR',action)
        terms = old_state.get_terms()
        new_term_values = old_state.get_state()  #  in case there are no modification to one of the terms
        #TODO make it more general
        terms_check = {}
        for k,v in invariants.iteritems():
            terms_check.update({k : False})
        for l in actions_list:
            single_action =  l.split(",")
            for i in single_action:
                for x in terms:
                    if x in i:
                        if 'VM' == x:
                            new_term_values[x] = i.split()[1] if i.split()[1].isdigit() else str(
                                (int(old_state.get_term_value(x))) + int(i.split()[1]))
                        else:
                            new_term_values[x] = i.split()[1] if i.split()[1].isdigit() else str((float(old_state.get_term_value(x))) + float(i.split()[1]))
            for k,v in invariants.iteritems():
                if ast.literal_eval(invariants[k][0]) <= ast.literal_eval(new_term_values[k]) <=  ast.literal_eval(invariants[k][1]):
                    terms_check[k] = True
            if all(terms_check.values()):
                new_state = SlaState(new_term_values)
                if new_state.get_state_label() in self.states: # If this state has been alreafy created don't create a new one
                    new_state = self.states[new_state.get_state_label()]
                else:
                    self.states.update({new_state.get_state_label() : new_state})
                new_states.append(new_state)
            else:
                new_states.append(None)
        return new_states

    #@timecall
    def sla_fsm_evolution(self, ini_state, sla, inv, broker=False):
        """
        Compute the SLA state evolution
        :param ini_state: initial state
        :param sla: SLA to be analysed
        """
        if broker:                          #Needed to compute the proposedSLA
            self.curr_owner = 'broker'
            self.curr_section = 'broker'
        check = True  # If FALSE all possible transitions have been analyzed
        states = []  # Keep the set of all the states generated
        temp_states = [ini_state]  # Keep the set of the temporary states generated
        self.states.update({ ini_state.get_state_label() :ini_state})
        invariants = inv
        g = nx.MultiDiGraph(name=self.curr_owner)
        g.add_node(ini_state)
        while check:
            for s in temp_states:
                for i in sla:
                    if eval(self.sla_fsm_condition_eval(s, i[1])):
                        found = False  # if TRUE the new state generated by a transition has already generated by another state
                        new_states = self.__sla_fsm_state_update(s, i[2],
                                                                invariants)  # Update the transition set of the old state and create the new possible states
                        for new_state in new_states:
                            if new_state != None:
                                if self.check_state(new_state, temp_states,
                                                    states):  # To avoid to iterate on the same states
                                    new_state = self.retrieve_state(new_state, temp_states + states)
                                    found = True
                                if not found:
                                    temp_states.append(new_state)
                                #TODO Now I am not considering the add remove actions
                                g.add_edge(s, new_state, label=i[0] ,
                                           action=utilities.edge_label_update((s, new_state),s.get_terms()),
                                           type='*',
                                           negotiator=i[3],
                                           agent=i[4],
                                           act_under='')
                                g.node[s]['label'] = s.get_state_label()
                                g.node[new_state]['label'] = new_state.get_state_label()
                    else:
                        pass
                states.append(s)  # Updating the list of permanent states
                temp_states.remove(s)
            if len(temp_states) == 0:  # Needed to iterate again over the temp list
                check = False
        utilities.save_graph(g, str(2) + self.curr_owner + 'SLA')
        self.automata[self.curr_owner].update({'SLA': g})  # Saving the SLA FSM


    def sla_fsm_break_cycles(self,edges):
        """
        Function to break the cycle in forward and backward paths
        :param edges:
        :return:
        """
        paths = []
        actions = set()
        for i in edges:
            actions.add(i[2]['label'])
        for i in actions:
            paths.append([n for n in edges if n[2]['label'] == i])
        return paths

    #@timecall
    def sla_fsm_evolution_alternative_paths(self):
        """
        To develop all the possible alternative paths for each action
        :return:
        """
        dict_idx = 0
        for i in self.parties:
            original_graph = self.automata[i]['SLA']
            complete_g = nx.MultiDiGraph(name=str(i) + 'complete_sla')
            complete_g.add_edges_from(original_graph.edges(data=True))
            negotiator = i
            opponent_agent = [n for n in self.parties if n != i][0]
            curr_edges = [n for n in original_graph.edges(data=True) if n[2]['agent'] == opponent_agent]
            current_sla = nx.MultiDiGraph(name=str(i) + '_action_selection', agent=opponent_agent)
            current_sla.add_edges_from(curr_edges)
            sub_graphs = list(nx.weakly_connected_component_subgraphs(current_sla,copy=True))
            for l in sub_graphs:  # TODO Maybe I can remove this step
                graphs = []  # To keep all the possible paths given by breaking the cycles
                original_sg = nx.MultiDiGraph()
                original_sg.add_edges_from(utilities.update_edge_nodes(l.edges(data=True), self.states))
                for_back = self.sla_fsm_break_cycles(original_sg.edges(data=True))
                for x in for_back:
                    graphs.append(nx.MultiDiGraph(x))
                for e in graphs:  # Iterate over the paths generated by the broken cycles and the normal paths
                    ini_nodes = utilities.get_ini_nodes(e)
                    out_nodes = utilities.get_end_nodes(e)
                    for y in ini_nodes:
                        for x in out_nodes:
                            paths = list(nx.all_simple_paths(original_sg,y,x))
                            for p in paths: #TODO maybe it is not needed for each path
                                for id, j in enumerate(p[:-1]):
                                    sat_actions = set()  # To keep the action underneath the saturation edge TODO REMOVE
                                    sat_edges = []
                                    curr_node= j
                                    sat_edges.append((curr_node,p[id+1],current_sla.get_edge_data(curr_node, p[id+1])[0]))
                                    sat_actions.add(current_sla.get_edge_data(curr_node, p[id+1])[0]['label'])
                                    id1 = id+1
                                    for m in p[id+1:]:
                                        if not current_sla.has_edge(curr_node, m) and not complete_g.has_edge(curr_node,m): #TODO check if we need to use current_sla
                                            sat_actions.add(current_sla.get_edge_data(p[id1-1], m)[0]['label'])
                                            sat_edges.append(
                                                (p[id1-1], m, current_sla.get_edge_data(p[id1-1], m)[0]))
                                            complete_g.add_edge(
                                                curr_node,
                                                m,
                                                label='SAT',
                                                action=self.edge_label_update((curr_node,m),curr_node.get_terms()),
                                                type='+',
                                                agent=opponent_agent,
                                                negotiator=negotiator,
                                                act_under=str(dict_idx))
                                            self.automata['sat_dict'].update({dict_idx : list(sat_edges)})
                                            dict_idx += 1
                                        id1 += 1
                                        if len([q for q in original_graph.out_edges(m, data=True) if q[2]['agent'] != opponent_agent]) != 0: #check weather the other party can interfer in the current node
                                            break
                                        else:
                                            pass
            for x in complete_g.nodes():
                complete_g.node[x]['label'] = self.update_node_label(x, self.states)
            utilities.save_graph(complete_g, str(i) + 'alternative_graph')
            self.automata[i]['COMPLETE_SLA'] = complete_g

    def check_state(self, state, temp_list, permanent_list):
        """ To avoid to check several times the same state"""
        t_states = [i.get_state() for i in temp_list]
        p_states = [i.get_state() for i in permanent_list]
        s = state.get_state()
        if (s in t_states or s in p_states):
            return True
        else:
            return False

    def retrieve_state(self, state, state_list):
        for i in state_list:
            if state.get_state() == i.get_state():
                return i

    def match_state(self,state,state_list):
        res =  None
        for i in state_list:
            if i.get_state_label() == state.get_state_label():
                res = i
        if not res:
            res = state
        return res


class SlaManagement:

    def __init__(self, p_data, conf_filename):
        config = ConfigObj(conf_filename, list_values=False)             # Reading the SLA file
        self.p_data = p_data
        self.n_terms = 0
        self.ini_ranges = ast.literal_eval(config['INI_RANGES']['ini_ranges'])
        self.up_ranges = ast.literal_eval(config['INI_RANGES']['up_ranges'])
        self.low_ranges = ast.literal_eval(config['INI_RANGES']['low_ranges'])
        self.sla_template = ast.literal_eval(config['SLA_TEMPLATE']['template'])
        self.terms = ast.literal_eval(config['TERMS']['terms'])
        self.provider_acts = ast.literal_eval(config['ACTIONS_TEMPLATE']['provider_act'])
        self.consumer_acts = ast.literal_eval(config['ACTIONS_TEMPLATE']['consumer_act'])
        self.action_template = ast.literal_eval(config['ACTIONS_TEMPLATE']['action_template'])
        self.provider_sla_list = []

    def define_action(self,actions_list,initial_terms):
        """
        Randomly instantiate the actions in the action list
        :param actions_list: the set of actions to be initialized
        :param initial_terms: the terms in the actions
        :return: None
        """
        action_signs = ['+', '-']
        for j in actions_list:
            term_defined = {}
            temp = []
            temp2 = []
            for i in self.terms:
                sign = random.choice(action_signs)
                action_term = self.action_template[i.upper()]
                condition_term = self.action_template[sign][i.upper()]
                inst_condition, inst_action = self.instantiate_action(condition_term, action_term, initial_terms[i])
                temp.append(inst_condition)
                temp2.append(str(i.upper()) + ' ' + str(sign) + str(inst_action))
                term_defined.update({i :{'condition' : inst_condition, 'action' : inst_action}})
            action = [(j[0], '(' + ' AND '.join(temp) + ')',','.join(temp2), j[1], j[2])]
            self.sla_template[j[1].lower()][j[2].upper()] += action


    def instantiate_action(self, condition, action, initial_term):
        """
        Instantiating a new action
        :param condition: the action condition
        :param action: the action template
        :param initial_term: the action terms
        :return: the conditiona and action initialized
        """
        if len(self.terms) > 2:
            nsteps = random.randint(13, 18) # For test in 06_02
        elif len(self.terms) == 1:
            nsteps = random.randint(1, 2)
        elif len(self.terms) == 2:
            nsteps = random.randint(2, 3)
        if isinstance(initial_term,int):
            int_off = random.randint(1, 2) # For test in 06_02
            if 'x1x' in action:
                action = action.replace('x1x', str(int_off))
            if 'ww' in condition:
                condition = condition.replace('ww', str(initial_term - 1))
            if 'ss' in condition:
                condition = condition.replace('ss', str(initial_term + 1))
            if 'i4i' in condition:  # only for 100% random sla
                condition = condition.replace('i4i', str(int(initial_term + nsteps * int_off + 1)))
            if 'h4h' in condition:  # only for 100% random sla
                if int(initial_term) - nsteps * int_off > 1:
                    condition = condition.replace('h4h', str(int(initial_term) - nsteps * int_off - 1))
                else:
                    condition = condition.replace('h4h', str(int_off - 1))
        else:
            float_off = float('%.1f' % random.uniform(0.1, 5.0))
            if 'y1y' in action:
                action = action.replace('y1y', str(float_off))
            if 'pp' in condition:
                condition = condition.replace('pp', str(initial_term - 0.1))
            if 'bb' in condition:
                condition = condition.replace('bb', str(initial_term + 0.1))
            if 'k4k' in condition:  # only for 100% random sla
                condition = condition.replace('k4k', str(float('%.1f' % float(initial_term + nsteps * float_off + 0.1))))
            if 'v4v' in condition:  # only for 100% random sla
                if initial_term - nsteps * float_off > 0.1:
                    condition = condition.replace('v4v', str(
                        float('%.1f' % float(initial_term - nsteps * float_off - 0.1))))
                else:
                    condition = condition.replace('v4v', str(float('%.1f' % float(float_off - 0.1))))
        return condition,action

    def instantiate_slas(self, sat):
        """
        Instantiate a sla for each consumer and provider
        :param file_out:
        :param sat: flag for transitivity closure
        :return: the lists of consumer and provider slas
        """
        n_prov = 0  # counting providers matched
        provider_list_no_sat = []
        provider_list_sat = []
        prov_nodes = 0
        prov_edges = 0
        prov_nodes_no_sat = 0
        prov_edges_no_sat = 0
        consumer_check = False
        stats = (0,0)
        avg_prov_nodes_nosat = 0.0
        avg_prov_edges_nosat = 0.0
        sla = dict()  # To save the SLA
        for i in self.p_data:
            sla_initial_state = SlaState(i, True)  # SLA initial state
            cons_sla = True
            self.sla_template['provider']['PROVIDER'] = []
            self.sla_template['provider']['CONSUMER'] = []
            self.sla_template['consumer']['PROVIDER'] = []
            self.sla_template['consumer']['CONSUMER'] = []
            n_prov += 1
            self.define_action(self.consumer_acts, i)
            self.define_action(self.provider_acts,i)
            if sat:
                sla_fsm_sat = SlaFsm(nx.copy.deepcopy(self.sla_template))
                provider_list_sat.append((nx.copy.deepcopy(self.sla_template),sla_fsm_sat.sla_fsm(sla_initial_state,1)))
                sla_fsm_no_sat = SlaFsm(nx.copy.deepcopy(self.sla_template))
                provider_list_no_sat.append((nx.copy.deepcopy(self.sla_template),sla_fsm_no_sat.sla_fsm(sla_initial_state,0)))
                stats_p = utilities.graph_statistics(sla_fsm_sat.sla_fsm_get_automata()['provider']['SLA'])
                stats_p_no_sat = utilities.graph_statistics(sla_fsm_no_sat.sla_fsm_get_automata()['provider']['SLA'])
                prov_nodes += stats_p[0]
                prov_edges += stats_p[1]
                prov_nodes_no_sat += stats_p_no_sat[0]
                prov_edges_no_sat += stats_p_no_sat[1]
            else:
                sla_fsm_no_sat = SlaFsm(nx.copy.deepcopy(self.sla_template))
                provider_list_no_sat.append((nx.copy.deepcopy(self.sla_template),sla_fsm_no_sat.sla_fsm(sla_initial_state, 0)))
                stats_p_no_sat = utilities.graph_statistics(sla_fsm_no_sat.sla_fsm_get_automata()['provider']['SLA'])
                prov_nodes_no_sat += stats_p_no_sat[0]
                prov_edges_no_sat += stats_p_no_sat[1]
            if not consumer_check:
                stats = utilities.graph_statistics(sla_fsm_no_sat.sla_fsm_get_automata()['consumer']['SLA'])
                print "Consumer nodes: " + str(stats[0])
                print "Consumer edges: " + str(stats[1])
                consumer_check = True
        if len(provider_list_no_sat) != 0:
            print "Average nodes : " + str(float(float(prov_nodes_no_sat) / float(len(provider_list_no_sat))))
            print "Average edges : " + str(float(float(prov_edges_no_sat) / float(len(provider_list_no_sat))))
            avg_prov_nodes_nosat = float(float(prov_nodes_no_sat) / float(len(provider_list_no_sat)))
            avg_prov_edges_nosat = float(float(prov_edges_no_sat) / float(len(provider_list_no_sat)))

        return provider_list_sat,provider_list_no_sat, stats[0], stats[1], avg_prov_nodes_nosat,avg_prov_edges_nosat