"""
This module contains some functions for the results evaluation and printing
"""

import collections
import agents


def results_eval(thresholds_model, utility_func_id, providers_list_sat, n_prov, provider_list_no_sat):
    """
    This function collects and evaluates the suggestions elaborated by the broker
    :param thresholds_model: the party's list of preference threshold
    :param utility_func_id:  the id of the utility function used
    :param providers_list_sat:  the list of providers with no saturation
    :param provider_list_no_sat: the list of providers with saturation activated
    :return:
    """
    pref_model = {'consumer': thresholds_model, 'provider': thresholds_model}
    results = {0: {}, 1: {}, 2: {}, 3: {}}
    nego_round = {0: {}, 1: {}, 2: {}, 3: {}}
    contractors = ['consumer', 'provider']
    print "Num providers " + str(len(provider_list_no_sat))
    no_nego_needed = 0
    prov_lists = {0 : provider_list_no_sat, 1: providers_list_sat}
    for k,l in prov_lists.iteritems():
        if len(l) > 0:
            for q, e in results.iteritems():
                for n in thresholds_model:
                    results[q].update({n: 0})
                    nego_round[q].update({n: 0})
            for p in l:
                print "----------- Negotiating with provider " + str(l.index(p)) + " -----------"
                broker = agents.SlaBroker(p[0], p[1], pref_model['consumer'], pref_model['provider'], utility_func_id)
                scores = broker.sla_broker_proposal_evaluation({'consumer': broker.automata['consumer']['SLA'],
                                                                'provider': broker.automata['provider']['SLA'],
                                                                'MATCHED': broker.automata['MATCHED']}, None, None)
                sugg = broker.sla_broker_elaborate_suggestions()
                broker.sla_broker_apply_multiple_suggestions(sugg, thresholds_model)
                for x, y in results.iteritems():
                    for z in y:
                        results[x][z] = results[x][z] + broker.results[x][z]
                        nego_round[x][z] = nego_round[x][z] + broker.negoRound[x][z]
            print " No nego needeed " + str(no_nego_needed)
            norm_nego_roud = normalize_nego_round(nego_round,n_prov)
            return dict(results), dict(norm_nego_roud)


def normalize_nego_round(nego_round, n_prov, f=None):
    """
    To define the average number of negotiation round among all the providers
    :param nego_round: number of negotiation round per utility function and per threshold
    :param n_prov: number of providers negotatiated with
    :param f: flag to save the results in a file
    :return: the avg number of negotiation round
    """
    for k,v in nego_round.iteritems():
        for l, m in v.iteritems():
            nego_round[k][l] = m / n_prov
    return nego_round


def print_results(results, f=None):
    """
    The print the matching results
    :param results: the resutls to be printed
    :param f: flag to save the results in a file
    :return: None
    """
    for k, v in results.items():
        print '\n\n --------------------------------------------------------'
        print 'Utility function ' + str(k)
        if f:
            f.write('\n\n --------------------------------------------------------\n'
                    'Utility function ' + str(k)+'\n')
        oresults = collections.OrderedDict(sorted(v.items()))
        for l, m in oresults.iteritems():
            print 'Threshold ' + str(l) + '\t Providers matched: ' + str(m)
            if f:
                f.write('Threshold ' + str(l) + '\t Providers matched: ' + str(m)+'\n')
    print '--------------------------------------------------------'


def print_nego_round(nego_round, n_prov, f=None):
    """
    To print the information about the negotiation round
    :param nego_round: Number of nego round per uf and threshold
    :param n_prov: number of providers
    :param f: flag to save the results in a file
    :return:
    """
    for k, v in nego_round.items():
        print '\n\n --------------------------------------------------------'
        print 'Average number of negotiaiton rounds needed for uf ' + str(k)
        if f:
            f.write('\n\n --------------------------------------------------------\n'
                    'Average number of negotiaiton rounds needed for uf ' + str(k)+'\n')
        onego_round = collections.OrderedDict(sorted(v.items()))
        for l, m in onego_round.iteritems():
            print 'Threshold ' + str(l) + '\t Avg_round: ' + str(float('%.2f' % float(float(m)/n_prov)))
            if f:
                f.write('Threshold ' + str(l) + '\t Avg_round: ' + str(float('%.2f' % float(float(m)/n_prov)))+'\n')
    print '--------------------------------------------------------'
