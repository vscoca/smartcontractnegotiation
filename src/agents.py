"""
This module contains all the classes modelling the agents involved in the negotiation process:
    - Negotiatior:
    - Broker
"""


import dsla_model_generator
import utilities
import networkx as nx
import numpy as np
import math


class SlaBroker(dsla_model_generator.SlaFsm):
    """
       This class contain all the routines used for the decision making process of a SLA proposal
    """
    def __init__(self, sla, automata,consumer_pref,provider_pref,utility_func_id):
        self.parties = ['consumer', 'provider']
        consumer_act = []
        provider_act = []
        #todo TO BE REMOVED
        self.preferences = consumer_pref
        for i in ['CONSUMER', 'PROVIDER']:
            consumer_act += sla['consumer'][i]
            provider_act += sla['provider'][i]
        self.sla = {'consumer': utilities.get_agents(consumer_act), 'provider': utilities.get_agents(provider_act), 'content' : sla ,'act_edited': []} # To keep also the action after the suggestions
        self.automata = automata  # To keep the automata to analyse
        self.states = self.sla_broker_retrieve_states()
        self.suggestionType = {'consumer':{'add':[],'remove':[]}, 'provider':{'add':[],'remove':[]}} # Used for the application of different solution preference
        self.negoRound = {0: {}, 1: {}, 2: {}, 3: {}}
        self.results = {0: {}, 1: {}, 2: {}, 3: {}}
        for q, e in self.results.iteritems():
            for n in self.preferences:
                self.results[q].update({n: 0})
                self.negoRound[q].update({n: 0})
        self.utilityFunctionId = utility_func_id

    def sla_broker_retrieve_states(self):
        states = {}
        for i in self.parties:
            for j in self.automata[i]['SLA'].nodes(data=True):
                states.update({j[1]['label'] : j[0]})
        return states

    def sla_broker_proposal_evaluation(self, automata, consumer_modifications, provider_modifications):
        """
        This method evaluates the suggestion to be applied
        :param automata: the automata to modify
        :param consumer_modifications:  the modifications to be applied to the consumer
        :param provider_modifications: the modifications to be applied to the provider
        :return: the scores for both consumer and provider
        """
        scores = {}
        for i in self.parties:
            agent_nodes = set([n for (n,v,z) in automata[i].edges(data=True) if z['agent'] == i
                               and z['negotiator'] == i.upper()])
            agent_nodes.update(set([v for (n,v,z) in automata[i].edges(data=True) if z['agent'] == i
                                    and z['negotiator'] == i.upper()]))
            matched_nodes = set([n for (n,v,z) in automata['MATCHED'].edges(data=True) if z['agent'] == i
                                 and z['negotiator'] == i.upper()])
            matched_nodes.update(set([v for (n,v,z) in automata['MATCHED'].edges(data=True) if z['agent'] == i
                                      and z['negotiator'] == i.upper()]))
            agent_transitions = [(n,v,z) for (n,v,z) in automata[i].edges(data=True) if z['agent'] == i
                                 and z['negotiator'].upper() == i.upper()]
            matched_transitions = [(n,v,z) for (n,v,z) in automata['MATCHED'].edges(data=True) if z['agent'] == i
                                   and z['negotiator'].upper() == i.upper()]
            if consumer_modifications != None:
                if i == 'consumer':
                    adaptation_cost =  utilities.adaptation_cost(consumer_modifications,automata['cons_edges'])
                else:
                    adaptation_cost = utilities.adaptation_cost(provider_modifications,automata['prov_edges'])
            else:
                adaptation_cost = 0.0
            smr = self.sla_borker_node_matching_ratio(agent_nodes, matched_nodes)
            tmr = self.sla_broker_transition_matching_ratio(agent_transitions,matched_transitions)
            scores.update({i:{'smr': smr, 'tmr':tmr, 'adaptation' : adaptation_cost}})
        return scores

    def sla_borker_node_matching_ratio(self, c_nodes, p_nodes):
        """
        Compute the State Matching Ratio (SMR)
        :param c_nodes: desired nodes
        :param p_nodes: matched nodes
        :return: the SMR
        """
        c_nodes_label = utilities.get_nodes_id(c_nodes)  # better to use the labels
        p_nodes_label = utilities.get_nodes_id(p_nodes)
        shared_nodes = set(c_nodes_label) & set(p_nodes_label)
        if len(shared_nodes) > 1:  # it is at least 1 due to the initial state
            ratio = float(
                float(len(shared_nodes)) / float(len(c_nodes))) # NOW THE INITIAL STATE IS TAKEN INTO ACCOUNT
        else:
            ratio = 0.0
        return ratio

    def sla_broker_transition_matching_ratio(self, o_transitions, m_transitions):
        """
        To compute the ratio between the original and the matched transitions.
        :param o_transitions: original transitions
        :param m_transitions: matched transitions
        :return: TMR
        """
        len_o = len(o_transitions)
        len_m = len(m_transitions)
        if len_m > 0 and len_o >0: #TODO 20/11 added len_o > 0
            ratio = float(float(len_m) / float(len_o))
        else:
            ratio = 0.0
        return ratio

    def sla_broker_elaborate_suggestions(self):
        """
        This function elaborates the set of suggestions to be applied to both consuemr and provider in order
        to improve the compatibility
        :return: (dict) The suggestions to be applied
        """
        message = ""
        general_solutions = {} # To keep all the possible modification to apply
        ini_state = self.sla_broker_get_initial_state(self.states)
        matched_edges = [n for n in self.automata['MATCHED'].edges(data=True)]
        y = 0
        for i in self.parties:  # To find wich are the paths connected with the matched actions
            opponent = [n for n in self.parties if n != i][0]
            missing_edges = [n for n in self.automata['MISSING'].edges(data=True) if
                             n[2]['agent'] == i and n[2]['negotiator'] == i.upper()]  # get all the active actions
            missing_paths = utilities.weakly_connected_components(nx.MultiDiGraph(missing_edges),self.states)
            for j in missing_paths:
                edges_to_add = []
                #self.print_path(self.graph_state2label(j)) #TODO FIX PRINT PATH FOR MULTIPLE PATHS
                connections = self.sla_broker_get_connection_nodes(j.edges(data=True), matched_edges, i, opponent) # To check if we need do modify an existing action
                node_connected = connections[0]
                different_node_connected = connections[1]
                if node_connected or different_node_connected: #if this path can be reached by direct actions
                    if node_connected: # In case I can modify a existing action that of the opponent
                        node_set = node_connected
                        edges_to_add += j.edges(data=True)
                        for k,v in node_connected.iteritems():
                            if v[1][opponent] == 'SAT':
                                message = "The " + opponent + " needs to modify " \
                                          + str(self.automata['sat_dict'][int(v[1]['act_under'])][0][2]['label'])
                            else:
                                message = "The " + opponent + " needs to modify " + v[1][opponent]
                            message_opp = "The " + i + " needs to modify " + v[1]['agent']
                        current_edges = matched_edges + j.edges(data=True)
                        action_id = opponent[0].upper() + str(y) + i[0].upper() + "m"
                        action_id_opp = opponent[0].upper() + str(y) + i[0].upper() + "d"
                    elif different_node_connected:
                        node_set = different_node_connected
                        edges_to_add += j.edges(data=True)
                        message =  "The " + opponent + " needs to add a new action sequences:"
                        self.suggestionType[opponent]['add'].append(edges_to_add)
                        message_opp = "The " + i + " needs to remove the  new action sequences:"
                        self.suggestionType[i]['remove'].append(edges_to_add)
                        current_edges = matched_edges + j.edges(data=True)
                        action_id = opponent[0].upper() + str(y) + i[0].upper() + "a"
                        action_id_opp = opponent[0].upper() + str(y) + i[0].upper() + "d"
                    if not self.sla_broker_check_connected_paths(nx.MultiDiGraph(current_edges), ini_state, j):
                        print "Not a valid solution yet"
                        break
                else:
                    message = "The following paths are not reachable in this SLA. They must be removed."
                    message_opp =  "The following paths are not reachable in this SLA. They must be removed."
                    action_id = opponent[0].upper() + str(y) + i[0].upper() + "rem"
                    action_id_opp = opponent[0].upper() + str(y) + i[0].upper() + "remN"
                    edges_to_add += j.edges(data=True)  # wrong name but this edges will be removed not added
                y += 1
                general_solutions.update({action_id: {'agent': i,
                                                      'negotiator': opponent,
                                                      'edges': edges_to_add,
                                                      'suggestion' : message,
                                                      'act_op' : action_id_opp}})
                general_solutions.update({action_id_opp: {'agent': i,
                                                          'negotiator': opponent,
                                                          'edges': edges_to_add,
                                                          'suggestion' : message_opp,
                                                          'act_op' : action_id}})
        return general_solutions


    def sla_broker_get_initial_state(self,states):
        """
        To revrive the initial state among all the input states
        :param states:
        :return: initial state
        """
        for k, v in states.iteritems():
            if v.check_initial():
                ini_state = v
        return ini_state

    def sla_broker_get_connection_nodes(self, missing_edges, matched_edges, agent, negotiator):
        """
        To check the graph connectivity for anomalies detection
        :param missing_edges: the list of not matched sequences
        :param matched_edges: the list of matched sequences
        :param agent: the current agent
        :param negotiator:  the opponent agent
        :return:
        """
        x = 0
        found = False
        matching_nodes = {}
        different_matching_nodes = {}
        missing_nodes = utilities.edges2nodes(missing_edges) # to check if the initial_state is in in the missing edges
        ini_state = self.sla_broker_get_initial_state(self.states) #TODO 22/11 aggiunto controllo su ini_nodes
        if len(utilities.get_ini_nodes(nx.MultiDiGraph(missing_edges))) !=0:
            ini_node = utilities.get_ini_nodes(nx.MultiDiGraph(missing_edges))[0]
        else:
            ini_node = None
        if ini_node:
            if ini_node.get_state_label() == ini_state.get_state_label():
                different_matching_nodes.update({x: (ini_state, {})})
            else:
                for j in matched_edges:
                    if ((ini_node.get_state_label() == j[1].get_state_label()) and agent == j[2]['agent'] and negotiator == j[2]['negotiator']):
                        matching_nodes.update({x: (j[1], j[2])})
                        found = True
                    if (ini_node.get_state_label() == j[1].get_state_label()):
                        different_matching_nodes.update({x: (j[1], j[2])})
                    x += 1
                if found:
                    different_matching_nodes.clear()
        else:
            if ini_state in missing_nodes:
                different_matching_nodes.update({x: (ini_state, {})})
            else:
                for i in missing_edges:
                    for j in matched_edges:
                        if (i[0].get_state_label() == j[1].get_state_label()
                            or i[1].get_state_label() == j[1].get_state_label()) \
                                and agent == j[2]['agent'] and negotiator == j[2]['negotiator']:
                            matching_nodes.update({x: (j[1], j[2])})
                            print 'FOUND'
                            found = True
                        if i[0].get_state_label() == j[1].get_state_label() or \
                                        i[1].get_state_label() == j[1].get_state_label():
                            different_matching_nodes.update({x: (j[1], j[2])})
                        x += 1
                if found:
                    different_matching_nodes.clear()
        return matching_nodes, different_matching_nodes

    def sla_broker_check_connected_paths(self,g,ini_state,missing_path):
        """
        To check if the applied solution is connected with the source node
        :param g: input graph
        :param ini_node:
        :param final_node:
        :return: true: the graph is connected, false otherwise
        """
        g.add_node(self.sla_broker_get_initial_state(self.states)) #To solve the bug related to missing initial node
        ini_nodes = utilities.get_ini_nodes(missing_path)
        if len(ini_nodes) > 0:
            nodes_set = ini_nodes
        else:
            nodes_set = [n[0] for n in missing_path.nodes(data=True)] # in case the missing path is a cycle
        if ini_state in nodes_set:
            check=True
        else:
            for i in nodes_set:
                print type(g)
                if not g.has_node(ini_state) or not g.has_node(i):
                    print 'Problem with nodes :' +str(ini_state.get_state_label()) +'\t'+str(ini_state) +'\t'+str(i.get_state_label()) +'\t'+str(i)
                    utilities.save_graph(self.automata['consumer']['SLA'], 'errorConsumer',True)
                    utilities.save_graph(self.automata['provider']['SLA'], 'errorProvider',True)
                    utilities.save_graph(self.automata['MISSING'], 'errorMissing', True)
                    utilities.save_graph(self.automata['MATCHED'], 'errorMatched', True)
                    utilities.save_graph(nx.MultiDiGraph(missing_path), 'errorCurrentMissing', True)
                    utilities.save_graph(g, 'errorG',True)
                if nx.has_path(g,ini_state,i):
                    check=True
                    break
                else:
                    check=False
        return check

    def sla_broker_apply_multiple_suggestions(self, suggestions,thresholds):
        """
        Apply the suggestions
        :param suggestions: list of suggestions to be applied
        :param thresholds: list of thresholds to be tested
        :return: None
        """
        sugg_keys = [k for k, v in suggestions.iteritems()]
        #TODO add a method that generates the combination according to the policies expressed 27/01
        modifications = utilities.generate_combinations(sugg_keys)
        temp_to_verify = {0: nx.copy.deepcopy(thresholds),1:nx.copy.deepcopy(thresholds),2:nx.copy.deepcopy(thresholds),3:nx.copy.deepcopy(thresholds)}
        if len(modifications) < 8000:
            for j in modifications:
                automata ={}
                consumer_modifications = []
                provider_modifications = []
                automata_matched = nx.copy.deepcopy(self.automata['MATCHED'])
                automata_consumer = nx.copy.deepcopy(self.automata['consumer']['SLA'])
                original_consumer_edg = len(automata_consumer.edges())
                automata_provider = nx.copy.deepcopy(self.automata['provider']['SLA'])
                original_provider_edg = len(automata_consumer.edges())
                current_modifications = {}
                for n in j:
                    current_modifications.update({n:suggestions[n]})
                for k,v in current_modifications.iteritems():
                    if v['edges'][0][2]['agent'] == 'consumer':
                        automata_agent = automata_consumer
                        automata_opponent =  automata_provider
                        opponent = 'provider'
                    else:
                        automata_agent = automata_provider
                        automata_opponent = automata_consumer
                        opponent = 'consumer'
                    if (v['negotiator'] == 'consumer' and (k[-1] =='a' or k[-1]=='m')) or (v['negotiator'] == 'provider' and k[-1] =='d'):
                        consumer_modifications+=v['edges']
                    else:
                        provider_modifications+=v['edges']
                    if k[-1] == 'a' or k[-1] == 'm':
                        if len([n for n in automata_matched.nodes() if n.check_initial()]) == 0:
                            for r in self.parties:
                                print self.automata[r]['SLA']
                        ini_state_match = [n for n in automata_matched.nodes() if n.check_initial()][0]
                        s=0
                        for i in v['edges']:
                            s+=1
                            if isinstance(i, tuple):
                                automata_matched.add_edge(self.match_state(i[0],automata_matched.nodes()), self.match_state(i[1],automata_matched.nodes()), action=i[2]['action'], agent=i[2]['agent'],
                                                          negotiator=i[2]['negotiator'], label=k + "|" + str(i[2]['label']),
                                                          type="+")
                                automata_opponent.add_edge(self.match_state(i[0],automata_agent.nodes()), self.match_state(i[1],automata_agent.nodes()),action=i[2]['action'],agent=i[2]['agent'], negotiator=opponent.upper(),label=k, type="+" )

                            else:
                                automata_matched.add_edge(self.match_state(i['source'],automata_matched.nodes()), self.match_state(i['end'],automata_matched.nodes()), action=i['action'], agent=i['agent'],
                                                          negotiator=i['negotiator'], label=k + "|" + str(i['label']),
                                                          type="+")
                                automata_opponent.add_edge(self.match_state(i['source'], automata_agent.nodes()),
                                                        self.match_state(i['end'], automata_agent.nodes()),
                                                        action=i['action'], agent=i['agent'],
                                                        negotiator=opponent.upper(), label=k,
                                                        type="+")
                        automata_matched = utilities.clean_sparse_nodes(automata_matched,ini_state_match)
                    elif k[-1] == 'd':
                        if len([n for n in automata_matched.nodes() if n.check_initial()]) == 0:
                            for r in self.parties:
                                print self.automata[r]['SLA'].iteritems()
                        ini_state = [n for n in automata_agent.nodes() if n.check_initial()][0]
                        ini_state_match = [n for n in automata_matched.nodes() if n.check_initial()][0]
                        for i in v['edges']:
                            for l in automata_agent.edges(data=True):
                                if isinstance(i, tuple):
                                    if i[0].get_state_label() == l[0].get_state_label() and \
                                                    i[1].get_state_label() == l[1].get_state_label():
                                        automata_agent.remove_edge(l[0], l[1])
                                else:
                                    if i['source'].get_state_label() == l[0].get_state_label() and \
                                                    i['end'].get_state_label() == l[0].get_state_label():
                                        automata_agent.remove_edge(l['source'], l['end'])
                        utilities.clean_sparse_nodes(automata_agent,ini_state)
                        automata_matched = utilities.clean_sparse_nodes(automata_matched,ini_state_match)
                    else:
                        pass
                #TODO insert the results verification routine
                automata.update({'MATCHED': automata_matched, 'provider': automata_provider,
                                 'consumer': automata_consumer, 'cons_edges': original_consumer_edg,
                                 'prov_edges': original_provider_edg})
                curr_result ={ 'scores':  self.sla_broker_proposal_evaluation(automata,consumer_modifications,provider_modifications), 'automata' : automata}
                for k,v in temp_to_verify.iteritems():
                    to_remove = []
                    for x in v:
                        self.negoRound[k][x] += + 1
                        if self.sla_broker_solutions_evaluations_alternative(curr_result, x,k) or x == 0.0:
                            self.results[k].update({x: self.results[k][x] + 1})
                            to_remove.append(x)
                    for h in to_remove:
                        temp_to_verify[k].remove(h)
                elements_to_go = temp_to_verify.values()
                to_go = True
                for x in elements_to_go:
                    if len(x) > 0:
                        to_go = False
                if to_go:
                    return
        else:
            print "Too many solutions!!"

    #@timecall
    def sla_broker_solutions_evaluations_alternative(self,results,thresholds,idx):
        found = False
        max_score = 0.0
        values = []
        agents_satisfied = {'consumer': False, 'provider': False}
        for p in self.parties:
            if thresholds <= self.sla_broker_utility_function_alternative(results['scores'][p]['tmr'],results['scores'][p]['adaptation'],idx):
                agents_satisfied.update({p:True})
                values+= [self.sla_broker_utility_function_alternative(results['scores'][p]['smr'],results['scores'][p]['adaptation'],idx), self.sla_broker_utility_function_alternative(results['scores'][p]['tmr'],results['scores'][p]['adaptation'],idx) ]
        if agents_satisfied['consumer'] and agents_satisfied['provider']:
            if utilities.hmean(values) >= max_score:
                max_score = utilities.hmean(values)
        if max_score != 0.0:
            found = True
        return found

    def sla_broker_utility_function_alternative(self, metric, adaptation_cost, idx):
        """
        A function which computes the overall utility score
        :param metric: The metric to be evaluated
        :param adaptation_cost: the adapcation cost to be considered
        :param idx: the utility function id
        :return: utility score
        """
        if idx == 0:
            utility_score = metric - np.log10(abs((adaptation_cost))+1)
        elif idx == 1:
            utility_score = metric - float(adaptation_cost)
        elif idx == 2:
            utility_score = metric - np.log(abs((adaptation_cost))+1)
        elif idx == 3:
            utility_score = metric - 0.7*np.power(adaptation_cost,2)
        return utility_score

class Negotiatior():
    """
    The class representing the negotiator
    """

    def __init__(self, name, ranges, weights):
        self.name = name
        self.ranges = ranges
        self.weights = weights
        self.prev_scores = {}
        self.curr_scores = {}
        self.ini_offer = self.initial_offer()
        self.beta = self.beta_func()

    def issue_score(self, term, value):
        """
        This method defines new score for the input term
        :param term: input term
        :param value: the previous value
        :return: new score
        """
        if bool(self.ranges[term][2]): # if utility increases as value increase
            return float((value-self.ranges[term][0])/(self.ranges[term][1]-self.ranges[term][0]))
        else:
            return float((self.ranges[term][1]-value)/(self.ranges[term][1]-self.ranges[term][0]))

    def scoring_function(self, values):
        """
        Computes the evaluation score
        :param values: the input values
        :return:
        """
        score = 0
        for i in self.ranges.keys():
            score += self.issue_score(i,values[i])*self.weights[i]
            self.curr_scores.update({i: self.issue_score(i,values[i])*self.weights[i]})
        return score

    def initial_offer(self):
        """
        Computes the initial offer
        :return: initial offer
        """
        offer = {}
        for i in self.ranges.keys():
            if i != 'VM':
                offer.update({i: float("{0:.1f}".format(np.random.uniform(self.ranges[i][0],self.ranges[i][1])))})
            else:
                offer.update({i: np.random.randint(int(self.ranges[i][0]),int(self.ranges[i][1]))})
            self.prev_scores.update({i: self.issue_score(i, offer[i]) * self.weights[i]})
        return offer

    def offer_generation(self, curr_time):
        """
        Counteroffer generation function
        :param curr_time: current time
        :return: conunteroffer
        """
        counter_offer = {}
        for i in self.ranges.keys():
            if bool(self.ranges[i][2]):  # if utility increases as value increase
                if i != 'VM':
                    counter_offer.update({i: float("{0:.1f}".format(self.ranges[i][0] + ((1.0 - self.nego_function(curr_time)) * (self.ranges[i][1] - self.ranges[i][0]))))})
                else:
                    counter_offer.update({i: int(self.ranges[i][0] + ((1.0 - self.nego_function(curr_time)) * (self.ranges[i][1] - self.ranges[i][0])))})
            else:
                if i != 'VM':
                    counter_offer.update({i: float("{0:.1f}".format(self.ranges[i][0] + (self.nego_function(curr_time) * (self.ranges[i][1] - self.ranges[i][0]))))})
                else:
                    counter_offer.update({i: int(self.ranges[i][0] + (self.nego_function(curr_time) * (self.ranges[i][1] - self.ranges[i][0])))})
        self.prev_scores = dict(self.curr_scores)
        return counter_offer

    def nego_function(self, curr_time):
        """
        Defining the slope of the negotiation function according to the current time
        :param curr_time: current time
        :return: the new slope
        """
        alpha_str = str(float(1/(1 + math.exp(-self.beta * (curr_time - 0.5*self.get_deadline())))))
        return float(alpha_str)

    def beta_func(self):
        """
        The beta value of the counteroffer function
        :return:
        """
        return np.random.uniform(0.1,1.0)

    def offer_evaluation(self, offer_rec,curr_time):
        """
        Evaluation of the current offer
        :param offer_rec: received offer
        :param curr_time: current time
        :return: true if accepted, false otherwise
        """
        curr_value = self.scoring_function(offer_rec)
        counteroffer = self.offer_generation(curr_time)
        return curr_value >= self.scoring_function(counteroffer)

    def get_ini_offer(self):
        """
        Get the initial offer
        :return:
        """
        return dict(self.ini_offer)

    def get_deadline(self):
        """
        Get the initial deadline
        :return:
        """
        return self.weights['T']